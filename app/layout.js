import "./globals.scss"
import { Poppins, Plus_Jakarta_Sans } from "next/font/google"

import Header from "@/src/components/header/header"
import Footer from "@/src/components/footer/footer"

export const metadata = {
  title: "Task",
}

const poppins_init = Poppins({
  subsets: ["latin"],
  weight: ["400", "500", "600", "800"],
  variable: "--font-poppins",
})

const plus_jakarta_sans_init = Plus_Jakarta_Sans({
  subsets: ["latin"],
  weight: ["400", "500", "600", "800"],
  variable: "--font-plus-jakarta-sans",
})

export default function RootLayout({ children }) {
  return (
    <html>
      <body
        className={`${poppins_init.variable} ${plus_jakarta_sans_init.variable}`}
      >
        <Header />
        {children}
        <Footer />
      </body>
    </html>
  )
}
