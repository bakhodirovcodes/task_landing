import Banner from "@/src/components/UI/Banner/banner"
import Trusted from "@/src/components/UI/Trusted/trusted"
import Features from "@/src/components/UI/Features/features"
import Counts from "@/src/components/UI/Counts/counts"
import Portfolio from "@/src/components/UI/Portfolio/portfolio"
import Blog from "@/src/components/UI/Blog/blog"
import Testimonials from "@/src/components/UI/Testimonials/testimonials"
import Faq from "@/src/components/UI/Faq/faq"
import Contact from "@/src/components/UI/Contact/contact"
import "./page.css"

export default function Home() {
  return (
    <div className="app">
      <Banner />
      <Trusted />
      <Features />
      <Counts />
      <Portfolio />
      <Blog />
      <Testimonials />
      <Faq />
      <Contact />
    </div>
  )
}
