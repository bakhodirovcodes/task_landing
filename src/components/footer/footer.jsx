import React from "react"

import "./footer.scss"

const Footer = () => {
  return (
    <footer>
      <div className="container">
        <div className="footer">
          <p>© 2023 Ideapeel Inc. All Rights Reserved</p>
          <p>Privacy Policy | Terms and Conditions</p>
        </div>
      </div>
    </footer>
  )
}

export default Footer
