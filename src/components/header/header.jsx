import React from "react"
import Image from "next/image"

import "./header.scss"

const Header = () => {
  return (
    <header className="container">
      <Image src="/images/logo.svg" alt="logo" width={72} height={52} />

      <img className="menu-btn" src="/images/menu.svg" alt="menu" />

      <nav>
        <ul>
          <li>About</li>
          <li>Service</li>
          <li>Blogs</li>
          <li>FAQ</li>
        </ul>
        <button>Contact Me</button>
      </nav>
    </header>
  )
}

export default Header
