import React from "react"
import { Inter } from "next/font/google"

import "./trusted.scss"

const logos = [
  "/images/logo5.svg",
  "/images/logo4.svg",
  "/images/logo1.svg",
  "/images/logo2.svg",
  "/images/logo3.svg",
]

const inter_init = Inter({
  subsets: ["latin"],
  weight: ["500"],
  variable: "--font-inter",
})
const Trusted = () => {
  return (
    <div className="trusted">
      <div className="container">
        <p className={`title ${inter_init.variable}`}>Trusted By</p>
        <div className="logos">
          {logos?.map((el) => (
            <img key={el} src={el} alt="logo" />
          ))}
        </div>
      </div>
    </div>
  )
}

export default Trusted
