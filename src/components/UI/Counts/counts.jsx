import React from "react"

import "./counts.scss"

const coutns = [
  {
    id: 1,
    title: "12K+",
    text: "Project Complete",
  },
  {
    id: 2,
    title: "7K+",
    text: "Happy Client",
  },
  {
    id: 3,
    title: "10+",
    text: "Years Experience",
  },
  {
    id: 4,
    title: "270+",
    text: "Win Awards",
  },
]
const Counts = () => {
  return (
    <div className="counts">
      <div className="container">
        {coutns?.map((el) => (
          <div key={el.id} className="counts-info">
            <h2 className="title">{el.title}</h2>
            <p className="text">{el.text}</p>
          </div>
        ))}
      </div>
    </div>
  )
}

export default Counts
