import React from "react"

import "./banner.scss"

const Banner = () => {
  return (
    <div className="banner">
      <div className="container">
        <img src="/images/banner.png" alt="img" />
        <div className="banner-info">
          <h1 className="miami">Miami</h1>
          <h1 className="walker">
            Walke<span>r</span>
          </h1>
          <p className="text">
            Award Wining product designer based in Georgia. We create
            user-friendly interfaces for fast-growing startups.
          </p>
          <button>Book A Call</button>
        </div>
        <div className="res-banner-info">
          <h1 className="res-walker">
            Walke<span>r</span>
          </h1>
          <p className="res-text">
            Award Wining product designer based in Georgia. We create
            user-friendly interfaces for fast-growing startups.
          </p>
          <button>Book A Call</button>
        </div>
      </div>
    </div>
  )
}

export default Banner
