import React from "react"

import "./testimonials.scss"

const Testimonials = () => {
  return (
    <div className="test">
      <div className="container">
        <p className="test-title">Testimonials</p>
        <h2 className="test-info">What our customer say</h2>

        <div className="test-cards">
          <div className="test-card">
            <img src="/images/vusax.svg" alt="svg" />
            <h3 className="card-text">
              “Now, i can track my business activity with easier and have a
              great understandable to operate the products
            </h3>
            <div className="test-footer">
              <div className="person-info">
                <img src="/images/ellipse1.png" alt="ellipse" />
                <p className="persont-name">Janne Cooper</p>
              </div>
              <div className="info-rate">
                <img src="/images/star.svg" alt="start" />
                <p className="count">4.3</p>
              </div>
            </div>
          </div>
          <div className="test-card">
            <img src="/images/vusax.svg" alt="svg" />
            <h3 className="card-text">
              “Now, i can track my business activity with easier and have a
              great understandable to operate the products
            </h3>

            <div className="test-footer">
              <div className="person-info">
                <img src="/images/ellipse2.png" alt="ellipse" />
                <p className="persont-name">Cobocannaeru</p>
              </div>
              <div className="info-rate">
                <img src="/images/star.svg" alt="start" />
                <p className="count">3.5</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Testimonials
