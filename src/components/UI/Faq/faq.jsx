import React from "react"

import ExpandMoreIcon from "@mui/icons-material/ExpandMore"

import "./faq.scss"

const data = [
  { id: 1, title: "How can we help your business?" },
  { id: 2, title: "What are the advantages of Bisnext" },
  { id: 3, title: "Let’s find an office near you?" },
  { id: 4, title: "How IT consultancy experts work?" },
]

const Faq = () => {
  return (
    <div className="faq">
      <div className="container">
        <div className="faq-container">
          <div className="res-faq-info">
            <h3>FAQs</h3>
            <h2 className="info-title">
              Be Kind to Your Mind Question & Answer
            </h2>
            <p className="faq-text">
              All Your Qustions are in A document, in question and answer
              format, that introduces newcomers to a topic or answers common
              questions.
            </p>
          </div>
          <div className="carusel">
            {data?.map((el) => (
              <div key={el.id} className="carusel-box">
                <p className="car-text">{el.title}</p>
                <ExpandMoreIcon />
              </div>
            ))}
          </div>
          <div className="faq-info">
            <h3>FAQs</h3>
            <h2 className="info-title">
              Be Kind to Your Mind Question & Answer
            </h2>
            <p className="faq-text">
              All Your Qustions are in A document, in question and answer
              format, that introduces newcomers to a topic or answers common
              questions.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Faq
