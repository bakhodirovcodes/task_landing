import React from "react"

import "./contact.scss"

const Contact = () => {
  return (
    <div className="contact">
      <div className="container">
        <h1 className="contact-title">Have you project in mind?</h1>
        <h2 className="contact-info">Let’s Make Something Great Together!</h2>

        <div className="contact-with">
          <p className="with-text">CONTACT WITH US</p>
          <img className="arrow" src="/images/arrow.svg" alt="arrow" />
        </div>

        <img src="/images/logo.svg" alt="logo" />

        <div className="icons">
          <img src="/images/icons/facebok.svg" alt="icon" />
          <img src="/images/icons/in.svg" alt="icon" />
          <img src="/images/icons/instagram.svg" alt="icon" />
          <img src="/images/icons/be.svg" alt="icon" />
          <img src="/images/icons/xbox.svg" alt="icon" />
        </div>
      </div>
    </div>
  )
}

export default Contact
