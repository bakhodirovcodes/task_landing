import React from "react"

import "./features.scss"

const Features = () => {
  return (
    <div className="features">
      <div className="container">
        <div className="features-info">
          <h3>FEATURES</h3>
          <h2 className="info-title">Giving Your Business Some Great Ideas</h2>
          <p className="info-text">
            Every designer needs the right tools to do the perfect job.
            Thankfully, we can do that. I design products that are more than
            pretty. I make them shippable and usable.
          </p>
          <button>Contact US</button>
        </div>

        <div className="features-cards">
          <div className="first-col">
            <div className="box">
              <img src="/images/features/ft1.png" alt="ft" />
              <h3 className="ft-title">UX Experience </h3>
              <p className="ft-text">
                I design products that are more than pretty. I make them usable.
              </p>
            </div>
            <div className="box">
              <img src="/images/features/ft2.png" alt="ft" />
              <h3 className="ft-title">Product Analysis</h3>
              <p className="ft-text">
                I design products that are more than pretty. I make them usable.
              </p>
            </div>
          </div>
          <div className="second-col">
            <div className="box">
              <img src="/images/features/sd1.png" alt="ft" />
              <h3 className="ft-title">UI Design</h3>
              <p className="ft-text">
                I design products that are more than pretty. I make them usable.
              </p>
            </div>
            <div className="box">
              <img src="/images/features/sd2.png" alt="ft" />
              <h3 className="ft-title">Product Design</h3>
              <p className="ft-text">
                I design products that are more than pretty. I make them usable.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Features
