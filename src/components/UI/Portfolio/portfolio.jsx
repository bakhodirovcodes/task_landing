import React from "react"

import "./portfolio.scss"

const Portfolio = () => {
  return (
    <div className="portfolio">
      <div className="container">
        <p className="title">Portfolio</p>
        <div className="info">
          <h2>Some Of Our Best Works </h2>
          <button>Load More</button>
        </div>
        <div className="image-row">
          <div className="img-info">
            <p className="info-title">UI Design</p>
            <h3>Greenfy Website</h3>
          </div>
          <img className="first-img" src="/images/portfolio/first.png" />
          <img className="second-img" src="/images/portfolio/second.png" />
          <img className="third-img" src="/images/portfolio/third.png" />
        </div>

        <div className="image-row">
          <img className="third-img" src="/images/portfolio/third.png" />
          <img className="second-img" src="/images/portfolio/second.png" />
          <img className="first-img" src="/images/portfolio/first.png" />
        </div>
        <div className="image-row">
          <img className="first-img" src="/images/portfolio/first.png" />
          <img className="second-img" src="/images/portfolio/second.png" />
          <img className="third-img" src="/images/portfolio/third.png" />
        </div>
      </div>
    </div>
  )
}

export default Portfolio
