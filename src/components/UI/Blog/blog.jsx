import React from "react"

import "./blog.scss"

const Blog = () => {
  return (
    <div className="blog">
      <div className="container">
        <div className="blog-container">
          <div className="blog-header">
            <p className="title">LATEST BLOG</p>
            <div className="blog-info">
              <h2>Check Some of Latest News & Articles</h2>
              <button>Load More</button>
            </div>
          </div>

          <div className="blog-cards">
            <div className="card">
              {[1, 2, 3, 4, 5, 6]?.map((el) => (
                <div key={el} className="box">
                  <img
                    className="main-img"
                    src="/images/cardImg.png"
                    alt="card"
                  />
                  <div className="card-info">
                    <p className="header">UI Design | 22 May 2023</p>
                    <h3 className="card-title">
                      What is UI Design in Front End Design?
                    </h3>
                    <p className="card-text">
                      But I must explain to you how all this mistaken idea of
                      denouncing pleasure and praising pain was born and I will
                      give you a complete account of the system
                    </p>
                    <span>
                      READ MORE <img src="/images/cardArrow.svg" alt="arrow" />
                    </span>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Blog
